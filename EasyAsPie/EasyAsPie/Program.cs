﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyAsPie
{
    class Program
    {
        public static void Main(string args[])
        {
            Kira.Find(objects.Where(i => i.Tags.Contains(LoadTag()) && i.Circumference / i.Diameter == Math.PI && i.ProductionSchedule == Schedules.Annual && i.Size < Breadbox.Size && i.Capacity== 4700000));
        }


        private static Tag LoadTag()
        {
            var animals = new Animals[] { Lamb, GreatBeast, SurfeitOfSeals };
            var A = FibonacciSequence[8];
            var B = Books.Where(i => i.Author.FirstName == FamousMovies.Birdemic.Director.FirstName&& i.Author.LastName==FamousNovels.TurningWheels.FirstName && i.Name.Meaning.FromLatin ==“book”);
            var C = Books.Where(i => this.ContainsAllAnimals(animals, i.Animals));
            var D = Months.Where(i => i.Letters.Count() == i.Calendar.Position).Calendar.Position;
            return B[C[A - 1][D - 1]].Words.Last();
        }

        private static bool ContainsAllAnimals(IENumerable<Animals> searchValues, IENumerable<Animals> contents)
        {
            if (contents == null || searchValues == null)

            {
                return false;
            }
            foreach (var animal in searchValues)
            {
                if (!contents.Contains(animal))
                {
                    return false;
                }
            }
            return true;

        }

    }
}
